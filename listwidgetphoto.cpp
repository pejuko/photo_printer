#include "listwidgetphoto.h"
#include "listwidgetphotoitem.h"

#include <iostream>

ListWidgetPhoto::ListWidgetPhoto(QWidget *parent) :
    QListWidget(parent),
    m_contextMenu(this)
{
    QAction *remove = new QAction(tr("Remove"), this);
    connect(remove, SIGNAL(triggered()), this, SLOT(removeCurrentItem()));

    m_contextMenu.addAction(remove);
}

void ListWidgetPhoto::contextMenuEvent(QContextMenuEvent *e)
{
    if (count() > 0 && currentItem()) {
        m_contextMenu.exec(e->globalPos());
    }
}

void ListWidgetPhoto::removeCurrentItem(void)
{
    ListWidgetPhotoItem *pitem = dynamic_cast<ListWidgetPhotoItem*>(currentItem());
    delete pitem->photo();
    removeItemWidget(pitem);
    delete pitem;
    emit itemRemoved();
}

void ListWidgetPhoto::dropEvent(QDropEvent *event)
{
    QListWidget::dropEvent(event);
    event->accept();
    emit orderChanged();
}

QList<ListWidgetPhotoItem*> *
ListWidgetPhoto::items(void)
{
    QList<QListWidgetItem*> tmp(findItems("*", Qt::MatchWildcard | Qt::MatchWrap));
    QList<ListWidgetPhotoItem*> *list = new QList<ListWidgetPhotoItem*>();

    for (int i=0; i<tmp.size(); i++) {
        list->push_back(dynamic_cast<ListWidgetPhotoItem*>(tmp[i]));
    }

    return list;
}
