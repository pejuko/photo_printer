#include "paper.h"
#include "pageview.h"
#include "ui_pageview.h"

#include <QPixmap>
#include <QPainter>

PageView::PageView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PageView),
    p_image(0),
    m_empty(0)
{
    ui->setupUi(this);
}

PageView::~PageView()
{
    if (p_image) delete p_image;
    delete ui;
}

void PageView::paintEvent(QPaintEvent *event)
{
    if (! p_image) return;
    QPixmap scaled = p_image->scaled(QSize(width(), height()), Qt::KeepAspectRatio);
    int x = (width() - scaled.width()) / 2;
    int y = (height() - scaled.height()) / 2;
    QPainter painter(this);
    painter.setViewport(0, 0, width(), height());
    painter.drawPixmap(x,y,scaled);
    painter.drawRect(QRect(x,y, scaled.width()-1, scaled.height()-1));
}

void PageView::resizeEvent(QResizeEvent *event)
{
    update();
}

void PageView::showPage(QPixmap *pixmap)
{
    if (p_image) delete p_image;
    p_image = new QPixmap(*pixmap);
    m_empty = 0;
    update();
}

void PageView::showPage(Paper const& paper, int orientation)
{
    QPixmap *pixmap = new QPixmap(paper.size(100, QPrinter::Orientation(orientation)));
    pixmap->fill();
    showPage(pixmap);
    delete pixmap;
    m_empty = 1;
}
