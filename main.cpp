#include <QtWidgets/QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("qPhotoPrinter");
    QCoreApplication::setApplicationName("qPhotoPrinter");

    QApplication a(argc, argv);
    MainWindow w(0, &a);

    w.show();

    return a.exec();
}
