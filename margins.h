#ifndef MARGINS_H
#define MARGINS_H

#include <QMargins>
#include <QPrinter>

class Margins
{
public:
    Margins(float left, float top, float right, float bottom, float gaps, Units unit) :
        m_left(left), m_top(top), m_right(right), m_bottom(bottom), m_gaps(gaps), m_unit(unit) {};

    float left() const { return m_left; };
    float left(int dpi) const { return m_unit.toDPI(dpi, m_left); };
    float top(int dpi) const { return m_unit.toDPI(dpi, m_top); };
    float top() const { return m_top; };
    float right(int dpi) const { return m_unit.toDPI(dpi, m_right); };
    float right() const { return m_right; };
    float bottom(int dpi) const { return m_unit.toDPI(dpi, m_bottom); };
    float bottom() const { return m_bottom; };
    float gaps() const { return m_gaps; };
    float gaps(int dpi) const { return m_unit.toDPI(dpi, m_gaps); };

private:
    float m_left, m_top, m_right, m_bottom;
    float m_gaps;
    Units m_unit;
};

#endif // MARGINS_H
