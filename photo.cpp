#include "photo.h"

class QImage;
class QPixmap;

Photo::Photo(QString const& fname, int size)
    : m_fileInfo(fname),
      m_width(m_preview.width()),
      m_height(m_preview.height()),
      m_previewSize(size)
{
    m_preview.convertFromImage(QImage(fname).scaled(m_previewSize, m_previewSize, Qt::KeepAspectRatio));
    m_thumbnail = m_preview.scaled(60, 60, Qt::KeepAspectRatio);
}

Photo::~Photo()
{
}

