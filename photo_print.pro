#-------------------------------------------------
#
# Project created by QtCreator 2011-09-25T17:38:03
#
#-------------------------------------------------

QT       += core gui widgets printsupport

TARGET = photo_print
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    photo.cpp \
    printer.cpp \
    paper.cpp \
    units.cpp \
    pageview.cpp \
    listwidgetphotoitem.cpp \
    listwidgetphoto.cpp \
    listwidgetsheetitem.cpp

HEADERS  += mainwindow.h \
    photo.h \
    printer.h \
    paper.h \
    margins.h \
    units.h \
    pageview.h \
    listwidgetphotoitem.h \
    listwidgetphoto.h \
    listwidgetsheetitem.h

FORMS += mainwindow.ui \
    pageview.ui


LIBS += -llcms2

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../lcms2/bin/ -llcms2
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../lcms2/bin/ -llcms2d

INCLUDEPATH += $$PWD/../lcms2/include
DEPENDPATH += $$PWD/../lcms2/include
