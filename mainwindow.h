#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "printer.h"
#include "pageview.h"

#include <QMainWindow>
#include <QModelIndexList>

namespace Ui {
    class MainWindow;
    class QModelIndexList;
    class PageView;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0, QApplication *app=0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Printer m_printer;
    Units m_units;
    bool m_canUpdate;
    int m_currentPage;
    QList<QPixmap*> *m_sheets;
    QApplication *m_app;

public slots:
    void openFiles(void);
    void print(void);
    void printToFile(void);
    void updatePageThumbnails(void);
    void updatePageView(void);
    void updateView(void);
    void unitsChanged(int);
    void pageChanged(void);
    void loadSettings(void);
    void saveSettings(void);
    void getSourceProfile(void);
    void getTargetProfile(void);
    void indexChanged(void);
    void photoRemoved(void);
    void currentPhotoChanged(int);

protected:
//    bool eventFilter(QObject *obj, QEvent *ev);
};

#endif // MAINWINDOW_H
