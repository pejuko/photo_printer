#ifndef LISTWIDGETSHEETITEM_H
#define LISTWIDGETSHEETITEM_H

#include <QListWidgetItem>

class ListWidgetSheetItem : public QListWidgetItem
{
public:
    explicit ListWidgetSheetItem(QPixmap *icon, int page_number);
};

#endif // LISTWIDGETSHEETITEM_H
