#include "printer.h"
#include "photo.h"
#include "paper.h"

#include <lcms2.h>

#include <iostream>

#include <QStringList>
#include <QPixmap>
#include <QPainter>
#include <QColor>
#include <QList>

class QStringList;
class QPixmap;
class QPainter;

Printer::Printer()
    : m_unit(QPrinter::Millimeter)
{
    m_papers << new Paper("10x15 (4x6)", 4.0, 6.0, QPrinter::Custom, QPrinter::Inch);
    m_papers << new Paper("A6", 105, 148.5, QPrinter::A6, QPrinter::Millimeter);
    m_papers << new Paper("A5", 148.5, 210.0, QPrinter::A5, QPrinter::Millimeter);
    m_papers << new Paper("A4", 210.0, 297.0, QPrinter::A4, QPrinter::Millimeter);
    m_papers << new Paper("A3", 297.0, 420.0, QPrinter::A3, QPrinter::Millimeter);
    m_papers << new Paper("A3+", 330.0, 483.0, QPrinter::Custom, QPrinter::Millimeter);
    m_papers << new Paper("Letter", 8.5, 11.0, QPrinter::Letter, QPrinter::Inch);
}

Printer::~Printer()
{
    while (! m_photos.isEmpty()) { delete m_photos.takeFirst(); }
    while (! m_papers.isEmpty()) { delete m_papers.takeFirst(); }
}

void Printer::loadPhotos(QStringList const& files, int size)
{
    QStringList::const_iterator i;

    for (i=files.constBegin(); i!=files.constEnd(); i++) {
        Photo *photo = new Photo(*i, size);
        m_photos << photo;
    }
}


int Printer::pageNumber(int photo_idx, int rows, int columns)
{
    return photo_idx / (rows * columns);
}

void Printer::setPhotos(QList<Photo*> *photos)
{
    m_photos.clear();
    QList<Photo*>::iterator i;
    for (i=photos->begin(); i<photos->end(); i++) {
        m_photos.push_back(*i);
    }
}

Photo const* Printer::findPhoto(QString const& fileName)
{
    QList<Photo*>::const_iterator i;
    for (i=m_photos.constBegin(); i!=m_photos.constEnd(); i++) {
        if ((*i)->fileName() == fileName)
            return (*i);
    }
    return 0;
}

Paper const* Printer::findPaper(QString const& name)
{
    QList<Paper*>::const_iterator i;
    for (i=m_papers.constBegin(); i!=m_papers.constEnd(); i++) {
        if ((*i)->name() == name)
            return (*i);
    }
    return 0;
}


QList<QPixmap*> * Printer::print(Paper const& paper, int dpi, int rows, int columns, Margins const& margins, int page, QFont const& font, int fontSize, int show_file_name, int orientation)
{
    QList<QPixmap*> *sheets = new QList<QPixmap*>();
    if (m_photos.empty())
        return sheets;

    int numSheets = m_photos.size() / (rows * columns);
    if ((numSheets * rows * columns) < m_photos.size())
        numSheets += 1;

    float l = margins.left(dpi), t = margins.top(dpi), r = margins.right(dpi), b = margins.bottom(dpi);
    float g = margins.gaps(dpi);
    QSize size = paper.size(dpi, QPrinter::Orientation(orientation));
    std::cout << "paper size: " << size.width() << "x" << size.height() << std::endl;
    QSize content(size.width() - l - r,
                  size.height() - t - b);

    QSize cell( int( (content.width()-(columns-1)*g) / columns ),
                int( (content.height()-(rows-1)*g) / rows ));

    QPixmap *tmp = new QPixmap(cell);
    QPainter *ptmp = new QPainter(tmp);
    int ldpi = tmp->logicalDpiX();
    QFont f(font);
    std::cout << "font size: " << fontSize << std::endl;
    f.setPixelSize( fontSize * double(dpi) / ldpi );
    std::cout << "font size: " << fontSize << std::endl;
    ptmp->setFont(f);
    //std::cout << "font = " << font().family().toStdString() << std::endl;
    QList<Photo*>::const_iterator photo = m_photos.constBegin();
    for (int i=0; i<numSheets; i++) {
        if (page != -1 && page != i) {
            photo += rows * columns;
            continue;
        }
        QPixmap *sheet = new QPixmap(size);
        sheet->fill();
        QPainter painter(sheet);
        painter.setViewport(0, 0, size.width(), size.height());
        painter.setFont(f);
        for (int r=0; r<rows; r++) {
            for (int c=0; c<columns; c++) {
                if (photo != m_photos.constEnd()) {
                    QPixmap img;
                    QRect text_box(0,0,0,0);
                    if (show_file_name)
                        ptmp->drawText(0,0,cell.width(),cell.height(),Qt::AlignHCenter,(*photo)->fileName(), &text_box);
                    QSize picture_cell(cell.width(), cell.height() - text_box.height());
                    if (cell.width()>(*photo)->preview().width()) {
                        std::cout << "preview width: " << (*photo)->preview().width() << " preview height: " << (*photo)->preview().height() << std::endl;
                        std::cout << "Load full image: " << (*photo)->fileName().toStdString() << std::endl;
                        img.convertFromImage(QImage((*photo)->filePath()).scaled(picture_cell, Qt::KeepAspectRatio));
                    } else {
                        img = (*photo)->preview().scaled(picture_cell, Qt::KeepAspectRatio);
                    }

                    int x = l + (cell.width()+g) * c;
                    int y = t + (cell.height()+g) * r;
                    int cx = x + (picture_cell.width() - img.width()) / 2;
                    int cy = y + (picture_cell.height() - img.height()) / 2;
                    int tx = x;
                    int ty = cy + img.height();
                    std::cout << "x = " << x << "   y = " << y << " cx = " << cx << " cy = " << cy << " tx = " << tx << " ty = " << ty << std::endl;

                    if (dpi>100)
                        painter.drawPixmap(cx, cy, convertCMS(&img));
                    else
                        painter.drawPixmap(cx, cy, img);

                    if (show_file_name)
                        painter.drawText(tx, ty, cell.width(), text_box.height(), Qt::AlignHCenter, (*photo)->fileName(), 0);

                    ++photo;
                }
            }
        }
        painter.save();
        sheets->append(sheet);
    }
    delete ptmp;
    delete tmp;

    return sheets;
}

QPixmap Printer::convertCMS(QPixmap *img)
{
    if (m_sourceProfile.isEmpty() || m_targetProfile.isEmpty())
        return *img;

    cmsHPROFILE in, out;
    cmsHTRANSFORM transform;

    in = cmsOpenProfileFromFile(m_sourceProfile.toStdString().c_str(), "r");
    out = cmsOpenProfileFromFile(m_targetProfile.toStdString().c_str(), "r");
    transform = cmsCreateTransform(in, TYPE_RGB_8, out, TYPE_RGB_8, INTENT_PERCEPTUAL, 0);

    cmsCloseProfile(in);
    cmsCloseProfile(out);

    QImage i(img->toImage());
    uchar *src_tmp = new uchar[i.width() * i.height() * 3];
    uchar *trg_tmp = new uchar[i.width() * i.height() * 3];

    int idx = 0;
    for (int y=0; y<i.height(); y++) {
        for (int x=0; x<i.width(); x++, idx+=3) {
            QColor pixel = i.pixel(x,y);
            src_tmp[idx] = pixel.red();
            src_tmp[idx+1] = pixel.green();
            src_tmp[idx+2] = pixel.blue();
        }
    }

    cmsDoTransform(transform, src_tmp, trg_tmp, i.width() * i.height());

    QImage iout(i.width(), i.height(), QImage::Format_RGB32);
    iout.fill(Qt::transparent);

    idx = 0;
    for (int y=0; y<i.height(); y++) {
        for (int x=0; x<i.width(); x++, idx+=3) {
            QColor color(trg_tmp[idx], trg_tmp[idx+1], trg_tmp[idx+2]);
            iout.setPixel(x,y,color.rgb());
        }
    }

    cmsDeleteTransform(transform);
    delete []src_tmp;
    delete []trg_tmp;

    return QPixmap::fromImage(iout);
}
