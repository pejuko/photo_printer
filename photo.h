#ifndef PHOTO_H
#define PHOTO_H

#include <QPixmap>
#include <QFileInfo>

class Photo
{
public:
    Photo(QString const& fname, int size=600);
    ~Photo();

    QString fileName() const { return m_fileInfo.fileName(); };
    QString filePath() const { return m_fileInfo.filePath(); };
    QPixmap const& preview() const { return m_preview; };
    QPixmap const& thumbnail() const { return m_thumbnail; };

private:
    QFileInfo m_fileInfo;
    QPixmap m_preview;
    QPixmap m_thumbnail;
    int m_width;
    int m_height;
    int m_previewSize;
};

#endif // PHOTO_H
