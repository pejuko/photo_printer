#ifndef UNITS_H
#define UNITS_H

#include <QSizeF>
#include <QMargins>
#include <QPrinter>

class Units
{
public:
    static QSizeF convert(QSizeF const& size, QPrinter::Unit from, QPrinter::Unit to);
    static QMargins convert(QMargins const& margins, QPrinter::Unit from, QPrinter::Unit to);
    static float convert(float size, QPrinter::Unit from, QPrinter::Unit to);
    static float step(QPrinter::Unit unit);

    Units() : m_unit(QPrinter::Millimeter) {};
    Units(QString unit);
    Units(QPrinter::Unit unit) : m_unit(unit) {};

    float toMM(float size) { return Units::convert(size, m_unit, QPrinter::Millimeter); };
    QSizeF toMM(QSizeF const& size) { return Units::convert(size, m_unit, QPrinter::Millimeter); };
    QMargins toMM(QMargins const& margins) { return Units::convert(margins, m_unit, QPrinter::Millimeter); };

    float toIN(float size) const { return Units::convert(size, m_unit, QPrinter::Inch); };
    QSizeF toIN(QSizeF const& size) const { return Units::convert(size, m_unit, QPrinter::Inch); };
    QMargins toIN(QMargins const& margins) const { return Units::convert(margins, m_unit, QPrinter::Inch); };

    float to(float size, Units const& unit) const { return Units::convert(size, m_unit, unit.unit()); };
    float to(float size, QPrinter::Unit unit) const { return Units::convert(size, m_unit, unit); };
    int toDPI(int dpi, float size) const { return toIN(size)*dpi; };

    float step() const { return Units::step(m_unit); };
    QPrinter::Unit unit() const { return m_unit; };

private:
    QPrinter::Unit m_unit;
};

#endif // UNITS_H
