#include <iostream>

#include "mainwindow.h"
#include "margins.h"
#include "ui_mainwindow.h"
#include "printer.h"
#include "listwidgetphotoitem.h"
#include "listwidgetsheetitem.h"

#include <QFileInfo>
#include <QDir>
#include <QFileDialog>
#include <QSettings>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QPixmap>
#include <QPaintEngine>
#include <QPainter>
#include <QDesktopWidget>

#include <iostream>

class QSettings;
class QPixmap;
class QPaintEngine;
class QPainter;
class QDesktopWidget;

MainWindow::MainWindow(QWidget *parent, QApplication *app) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_canUpdate(0),
    m_currentPage(-1),
    m_sheets(0),
    m_app(app)
{
    ui->setupUi(this);

    connect(ui->actionQuit, SIGNAL(triggered(bool)), this, SLOT(close()));
    connect(ui->actionAddImages, SIGNAL(triggered()), this, SLOT(openFiles()));
    connect(ui->actionPrint, SIGNAL(triggered()), this, SLOT(print()));
    connect(ui->actionPrintToFile, SIGNAL(triggered()), this, SLOT(printToFile()));
    connect(ui->buttonPrintToFile, SIGNAL(released()), this, SLOT(printToFile()));
    //connect(ui->listWidget, SIGNAL(itemSelectionChanged()), this, SLOT(updateView()));
    //connect(ui->paperUnit, SIGNAL(currentIndexChanged(int)), this, SLOT(unitsChanged(int)));

    QList<Paper*>::const_iterator paper;
    for (paper=m_printer.papers().constBegin(); paper!=m_printer.papers().constEnd(); paper++) {
        ui->paperSize->addItem((*paper)->name());
    }

    connect(ui->listWidget, SIGNAL(itemRemoved()), this, SLOT(photoRemoved()));
    connect(ui->listWidget, SIGNAL(orderChanged()), this, SLOT(indexChanged()));
    //ui->listWidget->installEventFilter(this);

    loadSettings();
}

MainWindow::~MainWindow()
{
    saveSettings();

    if (m_sheets) {
        while (! m_sheets->isEmpty()) delete m_sheets->takeFirst();
        delete m_sheets;
    }

    while (ui->listPages->count() > 0) delete ui->listPages->takeItem(0);
    ui->listPages->clear();

    delete ui;
}

void MainWindow::loadSettings(void)
{
    QSettings settings;

    ui->leftMargin->setValue(settings.value("margins/left", 5.0).toFloat());
    ui->topMargin->setValue(settings.value("margins/top", 5.0).toFloat());
    ui->rightMargin->setValue(settings.value("margins/right", 5.0).toFloat());
    ui->bottomMargin->setValue(settings.value("margins/bottom", 5.0).toFloat());
    ui->gaps->setValue(settings.value("margins/gaps", 5.0).toFloat());
    ui->orientation->setCurrentIndex(settings.value("page/orientation", 0).toInt());
    ui->rowsInput->setValue(settings.value("page/rows", 2).toInt());
    ui->columnsInput->setValue(settings.value("page/columns", 2).toInt());
    ui->paperSize->setCurrentIndex(settings.value("page/paperSize", 3).toInt());
    ui->paperUnit->setCurrentIndex(settings.value("page/paperUnit", 0).toInt());
    ui->dpi->setValue(settings.value("page/dpi", 300).toInt());
    ui->font->setCurrentFont(QFont(settings.value("font/family", "Sans Serif").toString()));
    ui->fontSize->setValue(settings.value("font/size", 10).toInt());
    ui->showFileName->setCheckState((settings.value("show/fileName", 2).toInt()==2) ? Qt::Checked : Qt::Unchecked);

    ui->splitter->restoreState(settings.value("window/splitter").toByteArray());
    ui->splitter->setChildrenCollapsible(true);

    if (settings.contains("window/size")) {
        restoreState(settings.value("window/state").toByteArray());
        resize(settings.value("window/size", QSize(800,600)).toSize());
    } else {
        setGeometry(m_app->desktop()->availableGeometry());
    }

    m_units = Units(ui->paperUnit->currentText());
    m_canUpdate = 1;
    updateView();
}


/*
bool MainWindow::eventFilter(QObject *obj, QEvent *ev)
{
    if (obj == ui->listWidget && ev->type() == QEvent::ChildRemoved) {
        indexChanged();
    }
    return false;
}
*/

void MainWindow::indexChanged(void)
{
    QList<Photo*> photos;
//    for (int i=0; i<m_printer.photos().size(); i++) {
    for (int i=0; i<ui->listWidget->count(); i++) {
        std::cout << i << std::endl;
        ListWidgetPhotoItem *item = dynamic_cast<ListWidgetPhotoItem*>(ui->listWidget->item(i));
//        ListWidgetPhotoItem *item = (ListWidgetPhotoItem*)(ui->listWidget->item(i));
        std::cout << item << std::endl;
        if (item)
            photos.push_back(item->photo());
    }
    m_printer.setPhotos(&photos);
    currentPhotoChanged(ui->listWidget->currentRow());
    updateView();
}

void MainWindow::photoRemoved(void)
{
    QList<Photo*> photos;
    for (int i=0; i<m_printer.photos().size()-1; i++) {
        ListWidgetPhotoItem *item = dynamic_cast<ListWidgetPhotoItem*>(ui->listWidget->item(i));
        photos.push_back(item->photo());
    }
    m_printer.setPhotos(&photos);
    currentPhotoChanged(ui->listWidget->currentRow());
    updateView();
}

void MainWindow::openFiles(void)
{
    QSettings settings;

    QStringList files = QFileDialog::getOpenFileNames(this, tr("Add Images"), settings.value("dir/openFiles", "").toString(), tr("Images (*.jpg *.png *.tif);;All Files (*)"), 0, 0);
    m_printer.loadPhotos(files, m_app->desktop()->availableGeometry().width() / 2);

    ui->statusBar->showMessage(tr("Loading photos..."));
    m_canUpdate = 0;
    ui->listWidget->clear();
    std::cout << "ListWidget.count() = " << ui->listWidget->count() << std::endl;
    QList<Photo*>::const_iterator i;
    for (i=m_printer.photos().constBegin(); i!=m_printer.photos().constEnd(); i++) {
        ListWidgetPhotoItem *item = new ListWidgetPhotoItem(*i);
        ui->listWidget->addItem(item);
    }
    ui->statusBar->showMessage(tr("Photos has been loaded."));
    std::cout << "ListWidget.count() = " << ui->listWidget->count() << std::endl;

    if (files.size() > 0) {
        QFileInfo finfo(*(files.begin()));
        settings.setValue("dir/openFiles", finfo.absoluteDir().absolutePath());
    }
    m_canUpdate = 1;
    updateView();
}


void MainWindow::getSourceProfile(void)
{
    QSettings settings;

    QString file_name = QFileDialog::getOpenFileName(this, tr("Source Profile"), settings.value("dir/sourceProfile","").toString(), tr("Profiles (*.icm *.icc)"), 0, 0);
    m_printer.setSourceProfile(file_name);
    QFileInfo finfo(file_name);
    settings.setValue("dir/sourceProfile", finfo.absoluteFilePath());
    ui->sourceProfile->setText(finfo.baseName());
}

void MainWindow::getTargetProfile(void)
{
    QSettings settings;

    QString file_name = QFileDialog::getOpenFileName(this, tr("Target Profile"), settings.value("dir/targetProfile","").toString(), tr("Profiles (*.icm *.icc)"), 0, 0);
    m_printer.setTargetProfile(file_name);
    QFileInfo finfo(file_name);
    settings.setValue("dir/targetProfile", finfo.absoluteFilePath());
    ui->targetProfile->setText(finfo.baseName());
}

void MainWindow::print()
{
}

void MainWindow::printToFile(void)
{
    QSettings settings;

    QString pattern = QFileDialog::getSaveFileName(this, tr("Print to File"), settings.value("dir/printFile", "").toString(), tr("Images (*.jpg *.png *tif);;All Files (*)"), 0, 0);
    if (pattern=="") return;

    QFileInfo file(pattern);
    QString dir = file.dir().absolutePath();
    QString base_name(file.baseName());
    QString prefix(dir+"/"+base_name);

    Paper const* paper = m_printer.findPaper(ui->paperSize->currentText());
    Units units(ui->paperUnit->currentText());
    Margins margins(ui->leftMargin->value(), ui->topMargin->value(), ui->rightMargin->value(), ui->bottomMargin->value(), ui->gaps->value(), units);

    ui->statusBar->showMessage(tr("Printing to file..."));
    QList<QPixmap*> *sheets = m_printer.print(*paper, ui->dpi->value(), ui->rowsInput->value(), ui->columnsInput->value(), margins, -1, ui->font->currentFont(), ui->fontSize->value(), ui->showFileName->checkState(), ui->orientation->currentIndex());
    std::cout << "Sheets: " << sheets->size() << std::endl;
    ui->statusBar->showMessage(tr("Printing to file has been finished."));

    int i=1;
    QList<QPixmap*>::const_iterator sheet;
    for (sheet=sheets->constBegin(); sheet!=sheets->constEnd(); sheet++, i++) {
        QString fnumber;
        fnumber.sprintf("-%05d.jpg", i);
        QString fname(prefix+fnumber);
        std::cout << "Print to: " << fname.toStdString() << std::endl;
        (*sheet)->save(fname);
    }
    while (! sheets->isEmpty()) delete sheets->takeFirst();
    delete sheets;

    settings.setValue("dir/printFile", pattern);
}

void MainWindow::unitsChanged(int idx)
{
    Units units(ui->paperUnit->currentText());

    m_canUpdate = 0;
    ui->leftMargin->setValue(m_units.to(ui->leftMargin->value(), units));
    ui->topMargin->setValue(m_units.to(ui->topMargin->value(), units));
    ui->rightMargin->setValue(m_units.to(ui->rightMargin->value(), units));
    ui->bottomMargin->setValue(m_units.to(ui->bottomMargin->value(), units));
    ui->gaps->setValue(m_units.to(ui->gaps->value(), units));

    ui->leftMargin->setSingleStep(units.step());
    ui->topMargin->setSingleStep(units.step());
    ui->rightMargin->setSingleStep(units.step());
    ui->bottomMargin->setSingleStep(units.step());
    ui->gaps->setSingleStep(units.step());
    m_canUpdate = 1;

    m_units = units;
}

void MainWindow::currentPhotoChanged(int row)
{
    int page = m_printer.pageNumber(row, ui->rowsInput->value(), ui->columnsInput->value());
    if (m_currentPage == page)
        return;
    m_currentPage = page;
    ui->listPages->setCurrentRow(m_currentPage);
    updatePageView();
}

void MainWindow::pageChanged()
{
    if (m_currentPage == ui->listPages->currentRow())
        return;
    m_currentPage = ui->listPages->currentRow();
    std::cout << "m_currentPage = " << m_currentPage << std::endl;
    updatePageView();
}

void MainWindow::updatePageThumbnails()
{
    Paper const* paper = m_printer.findPaper(ui->paperSize->currentText());
    Margins margins(ui->leftMargin->value(), ui->topMargin->value(), ui->rightMargin->value(), ui->bottomMargin->value(), ui->gaps->value(), m_units);

    while (ui->listPages->count() > 0) delete ui->listPages->takeItem(0);
    ui->listPages->clear();
    if (m_sheets) {
        while (! m_sheets->isEmpty()) delete m_sheets->takeFirst();
        delete m_sheets;
    }
    m_sheets = m_printer.print(*paper, 8, ui->rowsInput->value(), ui->columnsInput->value(), margins, -1, ui->font->currentFont(), ui->fontSize->value(), ui->showFileName->checkState(), ui->orientation->currentIndex());
    std::cout << "Sheets: " << m_sheets->size() << std::endl;

    if (m_sheets->count() == 0) {
        delete m_sheets;
        m_currentPage = -1;
        return;
    }

    if (m_currentPage >= m_sheets->count())
        m_currentPage = m_sheets->count() - 1;
    else if (m_currentPage == -1)
        m_currentPage = 0;

    QList<QPixmap*>::const_iterator sheet;
    int page_number = 1;
    for (sheet=m_sheets->constBegin(); sheet!=m_sheets->constEnd(); sheet++, page_number++) {
        ListWidgetSheetItem *item = new ListWidgetSheetItem(*sheet, page_number);
        ui->listPages->insertItem(page_number, item);
    }
    //scene->addRect(QRectF(0, 0, sheets->first()->width(), sheets->first()->height()));
    ui->listPages->setCurrentRow(m_currentPage);
}

void MainWindow::updatePageView(void)
{
    Margins margins(ui->leftMargin->value(), ui->topMargin->value(), ui->rightMargin->value(), ui->bottomMargin->value(), ui->gaps->value(), m_units);
    Paper const* paper = m_printer.findPaper(ui->paperSize->currentText());

    QList<QPixmap*> *sheets = m_printer.print(*paper, 100, ui->rowsInput->value(), ui->columnsInput->value(), margins, m_currentPage, ui->font->currentFont(), ui->fontSize->value(), ui->showFileName->checkState(), ui->orientation->currentIndex());
    std::cout << "sheets: " << sheets->count() << std::endl;

    if (! sheets->isEmpty()) {
        ui->pageView->showPage(sheets->first());
    }
    while (! sheets->isEmpty()) delete sheets->takeFirst();
    delete sheets;
}

void MainWindow::updateView(void)
{
    if (! m_canUpdate) return;

    if (m_printer.photos().count() == 0) {
        Paper const* paper = m_printer.findPaper(ui->paperSize->currentText());
        ui->pageView->showPage(*paper, ui->orientation->currentIndex());
        m_currentPage = -1;
        ui->listPages->clear();
        if (m_sheets) {
            while (! m_sheets->isEmpty()) delete m_sheets->takeFirst();
            delete m_sheets; m_sheets = 0;
        }
        return;
    }

    updatePageThumbnails();
    updatePageView();

    saveSettings();
}

void MainWindow::saveSettings(void)
{
    QSettings settings;
    settings.setValue("margins/left", ui->leftMargin->value());
    settings.setValue("margins/top", ui->topMargin->value());
    settings.setValue("margins/right", ui->rightMargin->value());
    settings.setValue("margins/bottom", ui->bottomMargin->value());
    settings.setValue("margins/gaps", ui->gaps->value());
    settings.setValue("page/rows", ui->rowsInput->value());
    settings.setValue("page/columns", ui->columnsInput->value());
    settings.setValue("page/orientation", ui->orientation->currentIndex());
    settings.setValue("page/paperSize", ui->paperSize->currentIndex());
    settings.setValue("page/paperUnit", ui->paperUnit->currentIndex());
    settings.setValue("page/dpi", ui->dpi->value());
    settings.setValue("font/family", ui->font->currentFont());
    settings.setValue("font/size", ui->fontSize->value());
    settings.setValue("show/fileName", ui->showFileName->checkState());
    settings.setValue("window/splitter", ui->splitter->saveState());
    settings.setValue("window/state", saveState());
    settings.setValue("window/size", size());
    settings.sync();
}
