#ifndef LISTWIDGETPHOTOITEM_H
#define LISTWIDGETPHOTOITEM_H

#include "photo.h"

#include <QListWidget>
#include <QListWidgetItem>

class ListWidgetPhotoItem : public QListWidgetItem
{
public:
    explicit ListWidgetPhotoItem(QListWidget *parent=0, int type=UserType) : QListWidgetItem(parent, type), p_photo(0) {}
    explicit ListWidgetPhotoItem(Photo *photo=0);

    void setPhoto(Photo *photo) { p_photo = photo; };
    Photo * photo(void) { return p_photo; };
    QListWidgetItem *clone(void);

private:
    Photo *p_photo;

};

#endif // LISTWIDGETPHOTOITEM_H
