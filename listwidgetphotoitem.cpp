#include "listwidgetphotoitem.h"
#include <QListWidgetItem>
#include <QListWidget>
#include <QDropEvent>
#include <iostream>

ListWidgetPhotoItem::ListWidgetPhotoItem(Photo *photo)
    : QListWidgetItem(0, UserType),
      p_photo(photo)
{
    setData(1, photo->thumbnail());
    setText(photo->fileName());
}

QListWidgetItem *ListWidgetPhotoItem::clone(void)
{
    ListWidgetPhotoItem *i = dynamic_cast<ListWidgetPhotoItem*>(QListWidgetItem::clone());

    i->setPhoto(p_photo);

    return dynamic_cast<ListWidgetPhotoItem*>(i);
}
