#include "listwidgetsheetitem.h"

#include <QString>
#include <QPixmap>
#include <QPainter>

ListWidgetSheetItem::ListWidgetSheetItem(QPixmap *icon, int page_number) :
    QListWidgetItem()
{
        QPixmap scaled = icon->scaled(QSize(81,81), Qt::KeepAspectRatio);

        QPixmap thumb(QSize(scaled.width()+2, scaled.height()+2));
        thumb.fill();
        QPainter painter(&thumb);
        painter.setViewport(0, 0, thumb.width(), thumb.height());
        painter.drawRect(QRect(0,0, thumb.width()-1, thumb.height()-1));
        painter.drawPixmap(1,1,scaled);

        setData(1, thumb);

        QString number;
        number.sprintf("%s %d", QObject::tr("Page").toStdString().c_str(), page_number);

        setToolTip(number);
        setText(number);
}
