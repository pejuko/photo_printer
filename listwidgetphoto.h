#ifndef LISTWIDGETPHOTO_H
#define LISTWIDGETPHOTO_H

#include "listwidgetphotoitem.h"
#include <QMenu>
#include <QListWidget>
#include <QContextMenuEvent>
#include <QList>

class ListWidgetPhoto : public QListWidget
{
    Q_OBJECT
public:
    explicit ListWidgetPhoto(QWidget *parent = 0);

    void dropEvent(QDropEvent *event);

    QList<ListWidgetPhotoItem*> *items(void);

protected:
    virtual void contextMenuEvent(QContextMenuEvent *e);

signals:
    void itemRemoved(void);
    void orderChanged(void);
    
public slots:
    void removeCurrentItem(void);

private:
    QMenu m_contextMenu;
};

#endif // LISTWIDGETPHOTO_H
