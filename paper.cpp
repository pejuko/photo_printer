#include "paper.h"
#include "units.h"

class QPrinter;
class QSizeF;
class QSize;

Paper::Paper(QString name, float width, float height, QPrinter::PaperSize format, QPrinter::Unit unit)
    : m_name(name),
      m_width(width),
      m_height(height),
      m_format(format),
      m_unit(unit)
{
}

QSizeF Paper::size(QPrinter::Unit unit, QPrinter::Orientation orientation) const
{
    float w = m_width;
    float h = m_height;

    if (orientation == QPrinter::Landscape) {
        w = m_height;
        h = m_width;
    }

    return QSizeF(m_unit.to(w, unit), m_unit.to(h, unit));
}

QSize Paper::size(int dpi, QPrinter::Orientation orientation) const
{
    QSizeF s = size(QPrinter::Inch, orientation);
    return QSize(int(s.width()*dpi), int(s.height()*dpi));
}
