#ifndef PAGEVIEW_H
#define PAGEVIEW_H

#include "paper.h"

#include <QWidget>

namespace Ui {
class PageView;
}

class PageView : public QWidget
{
    Q_OBJECT
    
public:
    explicit PageView(QWidget *parent = 0);
    ~PageView();

    void showPage(QPixmap *pixmap);
    void showPage(Paper const& paper, int orientation=0);

protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);
    
private:
    Ui::PageView *ui;
    QPixmap *p_image;
    bool m_empty;
};

#endif // PAGEVIEW_H
