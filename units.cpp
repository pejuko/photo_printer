#include "units.h"

#include <QSize>
#include <QMargins>
#include <QPrinter>


QSizeF Units::convert(QSizeF const& size, QPrinter::Unit from, QPrinter::Unit to)
{
    int width = Units::convert(float(size.width()), from, to);
    int height = Units::convert(float(size.height()), from, to);

    return QSize(width, height);
}

QMargins Units::convert(QMargins const& margins, QPrinter::Unit from, QPrinter::Unit to)
{
    float left = Units::convert(float(margins.left()), from, to);
    float top = Units::convert(float(margins.top()), from, to);
    float right = Units::convert(float(margins.right()), from, to);
    float bottom = Units::convert(float(margins.bottom()), from, to);

    return QMargins(left, top, right, bottom);
}

float Units::convert(float size, QPrinter::Unit from, QPrinter::Unit to)
{
    float q = 1.0;

    if (from != to) {
        switch(to) {
            case QPrinter::Millimeter:
                q = 25.4;
                break;
            case QPrinter::Inch:
                q = 1.0/25.4;
                break;
            default:
                q = 1.0;
                break;
        }
    }

    return size*q;
}

float Units::step(QPrinter::Unit unit)
{
    float step = 1.0;

    switch(unit) {
    case QPrinter::Millimeter:
        step = 1.0;
        break;
    case QPrinter::Inch:
        step = 0.1;
        break;
    default:
        step = 1.0;
        break;
    }

    return step;
}

Units::Units(QString unit)
{
    if (unit == "mm") m_unit = QPrinter::Millimeter;
    if (unit == "in") m_unit = QPrinter::Inch;
}
