#ifndef PRINTER_H
#define PRINTER_H

#include "photo.h"
#include "paper.h"
#include "margins.h"

#include <QFont>
#include <QList>
#include <QPixmap>
#include <QMargins>
#include <QModelIndexList>

class Printer
{
public:
    Printer();
    ~Printer();

    void loadPhotos(QStringList const& files, int size=600);

    void setSourceProfile(QString fname) { m_sourceProfile = fname; };
    void setTargetProfile(QString fname) { m_targetProfile = fname; };

    QList<Photo*> const& photos() const { return m_photos; };
    QList<Paper*> const& papers() const { return m_papers; };
    int pageNumber(int photo_idx, int rows, int columns);

    void setPhotos(QList<Photo*> *photos);

    Photo const* findPhoto(QString const& fileName);
    Paper const* findPaper(QString const& name);

    QList<QPixmap*> * print(Paper const& paper, int dpi, int rows, int columns, Margins const& margins, int page=-1, QFont const& font=QFont("Sans Serif"), int fontSize=12, int show_file_name=2, int orientation=0);

private:
    QPixmap convertCMS(QPixmap *img);

    QList<Photo*> m_photos;
    QList<Paper*> m_papers;
    QString m_sourceProfile;
    QString m_targetProfile;

    QPrinter::Unit m_unit;
};

#endif // PRINTER_H
