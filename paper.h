#ifndef PAPER_H
#define PAPER_H

#include "units.h"

#include <QPrinter>
#include <QSizeF>
#include <QSize>

class Paper
{
public:
    Paper(QString name, float width, float height, QPrinter::PaperSize format, QPrinter::Unit unit);

    QSizeF size(QPrinter::Unit unit, QPrinter::Orientation orientation=QPrinter::Portrait) const;
    QSize  size(int dpi, QPrinter::Orientation orientation=QPrinter::Portrait) const;

    QString name() const { return m_name; };

private:
    QString m_name;
    float m_width;
    float m_height;
    QPrinter::PaperSize m_format;
    Units m_unit;
};

#endif // PAPER_H
